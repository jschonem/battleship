# README #

This is a Python implementation of Battleship for AI competitions. It should also 
support a human interface but that's not currently set up yet.

### Getting Started ###

battleship.py contains the supporting functionality to run a game. A baseline AI is defined in
ai_agent.py; running this file will run a competition between two identical implementations
of the baseline. 

A mouse-interactive human agent can be used by running "human_agent.py". Currently this will
play against the baseline AI agent, which is not very good. The GUI and callbacks are implemented
through matplotlib so things aren't super smooth or clean. 

### Writing an AI ###

The class AI_Agent (in ai_agent.py) serves as a template to define a new AI. It inherits from
the abstract base class Agent (in battleship.py) which defines a set of abstract methods that
must be implemented. The four key methods that an agent must implement are 

* place_ships (selects ship location at start of game)
* shot_out (takes a shot during a turn)
* shot_in (returns info about a shot an opponent took)
* shot_info (provides information about the shot this agent took).

At the end of each game, the Game object (in battleship.py) associated with that game is stored
in the AI_Agent class property "game_results." This can be used to allow an agent to learn about 
its opponent, although doing so requires deeper investigation of the object model in battleship.py.

### Battleship Object Model ###

To be written. There's only 600 lines of code so it's not that hard to piece together if needed.
