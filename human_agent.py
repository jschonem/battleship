# -*- coding: utf-8 -*-
"""

Baseline human agent and demonstation of how to run against an AI agent.
All input is currently text-based, although the results of each turn are
plotted to screen. Run from a separate command window (not through Spyder)
for best results. 

"""

# TODO: Potentially make competition framework class-based

import numpy as np
from copy import copy
from random import randint, seed
from battleship import Agent, Ship, Shot, Game, coord_display, display_coord
from ai_agent import AI_Agent
import matplotlib.pyplot as plt

class Human_Agent(Agent):
    """
    Concrete reference implementation of a human agent which can receive 
    text-based input.
    """
    
    # Class property for Game result storage
    game_results = list()
    orientations  = ['E', 'S', 'W', 'N']
    
    def __init__(self, *args, **kwargs):
        
        # Superclass init
        super().__init__(*args, **kwargs)
        self.fig, self.ax = plt.subplots(2, 1, figsize = (5, 10))
        self.fig.canvas.set_window_title('BATTLESHIP')
        self.plot_opp(self.ax[0])
        self.plot(self.ax[1])
        
        # Ship/shot placement helpers
        self.current_ship = None
        self.current_ship_loc = [0, 0]
        self.current_ship_orient = 'E'
        self.current_shot_loc = [0, 0]
        self.current_shot = None
        self.event_cid = dict()
        
        
    def _ship_drag(self, event):
        """
        Moves ship around for placement. Active for bottom axis (ax[1]) only.
        """
        
        # Ship placement events only active for the bottom axis
        if event.inaxes != self.ax[1]:
            return
        
        col_ind = int(np.floor(event.xdata))
        row_ind = int(np.floor(event.ydata))
        
        if col_ind < 0 or row_ind < 0:
            return
        
        if (col_ind == self.current_ship_loc[0] and 
            row_ind == self.current_ship_loc[1]):
            return
        
        self.current_ship_loc[0] = row_ind
        self.current_ship_loc[1] = col_ind
        self.current_ship.set_location(self.current_ship_loc)

        self.current_ship.plot(ax = self.ax[1])
        plt.draw()
        
    def _ship_rotate(self, event):
        """
        Rotates ship if mouse is right clicked.
        """
        
        # This is hacky but I don't know where "MouseButton" lives
        if str(event.button) == 'MouseButton.RIGHT':
            orient_ind = self.orientations.index(self.current_ship_orient)
            orient_ind = (orient_ind + 1 ) % len(self.orientations)
            self.current_ship_orient = self.orientations[orient_ind]
            self.current_ship.set_orientation(self.current_ship_orient)
            
            self.current_ship.plot(ax = self.ax[1])
            plt.draw()
        
    def _ship_place(self, event, iterator):
        """
        Attempts to place ship in current location and orientation. If the 
        placement fails, no action is taken.
        """
        
        if str(event.button) == 'MouseButton.RIGHT':
            return
        
        # Attempt placement
        try:
            self.player_board.add_ship(self.current_ship,
                                       tuple(self.current_ship_loc),
                                       self.current_ship_orient)
            title_hold = self.ax[1].title
            self.ax[1].clear()
            self.player_board.plot(ax = self.ax[1])
            self.ax[1].set_title(title_hold.get_text(), 
                                 fontweight = title_hold.get_fontweight())
            plt.draw()
            for ship in self.ships:
                print(ship)
        except ValueError:
            print('ValueError')
            return
        
        # Attempt to iterate
        try:
            self.current_ship = self.ships[self.ships.index(self.current_ship) + 1]
            self.current_ship.set_location((self.current_ship_loc[0],
                                            self.current_ship_loc[1]))
            self.current_ship.set_orientation(self.current_ship_orient)
        except IndexError:
            # Disconnect events and continue (?)
            self.fig.canvas.mpl_disconnect(self.event_cid['drag'])
            self.fig.canvas.mpl_disconnect(self.event_cid['rotate'])
            self.fig.canvas.mpl_disconnect(self.event_cid['place'])
            self.fig.canvas.stop_event_loop()
            
    def _shot_drag(self, event):
        """
        Moves shot around for placement. Active for top axis (ax[0]) only.
        """
        
        # Shot placement events only active for the top axis
        if event.inaxes != self.ax[0]:
            return
        
        col_ind = int(np.floor(event.xdata))
        row_ind = int(np.floor(event.ydata))
        
        if (col_ind < 0 or row_ind < 0
            or col_ind >= self.opp_board.board_cols
            or row_ind >= self.opp_board.board_rows):
            return
        
        if (col_ind == self.current_shot_loc[0] and 
            row_ind == self.current_shot_loc[1]):
            return
        
        self.current_shot_loc[0] = row_ind
        self.current_shot_loc[1] = col_ind
        
        self.current_shot.set_coordinates(self.current_shot_loc)
        if self.opp_board.check_shot_exists(self.current_shot):
            return

        self.current_shot.plot(ax = self.ax[0])
        plt.draw()
        
    def _shot_place(self, event):
        """
        Attempts to place shot in current location. If the 
        placement fails, no action is taken. On succesful placement the
        event loop is ended.
        """
        
        if str(event.button) == 'MouseButton.RIGHT':
            return
        
        # Refuse placement if a shot exists in this location
        self.current_shot.set_coordinates(tuple(self.current_shot_loc))
        if self.opp_board.check_shot_exists(self.current_shot):
            return
        
        # Otherwise we can place
        self.opp_board.shot_set(self.current_shot)
        
        # Disconnect and return control
        self.fig.canvas.mpl_disconnect(self.event_cid['drag'])
        self.fig.canvas.mpl_disconnect(self.event_cid['place'])
        self.fig.canvas.stop_event_loop()
            
    def place_ships(self, debug = False):
        """
        Places ships per player input; plots each new ship upon placement.
        """
        
        # Queue up initial ship 
        self.current_ship = self.ships[0]
        self.current_ship.set_location(self.current_ship_loc)
        self.current_ship.set_orientation(self.current_ship_orient)
        self.current_ship.plot(ax = self.ax[1])
        
        # Set up an iterator to yield the remaining ships in sequence. I went
        # in circles a few times to get this graphical placement working. I
        # think this solution is probably more complex than it needs to be 
        # but for now it works. 
        def ship_iter():
            for ship in self.ships[1:]:
                print('Yielding %s' % str(ship))
                yield ship
        
        # Define a placement callback using this iterator
        place_fun = lambda event: self._ship_place(event, ship_iter())
        
        # Connect events
        self.event_cid['drag'] = self.fig.canvas.mpl_connect('motion_notify_event',
                                        self._ship_drag)
        self.event_cid['rotate'] = self.fig.canvas.mpl_connect('button_press_event',
                                    self._ship_rotate)
        self.event_cid['place'] = self.fig.canvas.mpl_connect('button_press_event',
                                    place_fun)
        self.ax[1].set_title('Left Click to Place Ships\nRight Click to Rotate',
              fontweight = 'bold')
        plt.show(block = False)
        self.fig.canvas.start_event_loop(timeout = 0)
        self.ax[1].set_title('')
        
# TODO: Press "enter" for random placement
#        
#        for ind, ship in enumerate(self.ships[ind:]):
#            while True:
#                row_ind = randint(0, self.player_board.board_rows - 1)
#                col_ind = randint(0, self.player_board.board_cols - 1)
#                orientation = ['N', 'S', 'E', 'W'][randint(0, 3)]
#                
#                try:
#                    self.player_board.add_ship(ship, (row_ind, 
#                                                      col_ind), 
#                                                      orientation)
#                    self.ax[1].clear()
#                    self.plot(self.ax[1])
#                    plt.pause(0.01)
#                    break
#                except ValueError:
#                    continue
        
    def shot_out(self, current_turn, debug_loc = False):
        """
        Takes a shot per player input. No plots are updated until we get
        the info on what happened to the shot. 
        """
        
#        print('TAKE SHOT FOR TURN %i' % (current_turn + 1))
#        while True:
#            raw_in = input('(ROW [LETTER], COL [NUMBER]) >>')
#            try:
#                coords = self._format_shot(raw_in)
#                new_shot = Shot(coords, current_turn)
#                self.opp_board.shot_set(new_shot)
#                break
#            except ValueError:
#                print('Invalid Input: %s' % raw_in)
#            
#        self.last_shot = new_shot
#        return new_shot
        
        # Create new shot object for this turn
        self.current_shot = Shot(self.current_shot_loc, current_turn)
        
        # Connect events and start placement event loop
        self.event_cid['drag'] = self.fig.canvas.mpl_connect('motion_notify_event',
                                        self._shot_drag)
        self.event_cid['place'] = self.fig.canvas.mpl_connect('button_press_event',
                                    self._shot_place)
        self.ax[0].set_title('Left Click to FIRE!', fontweight = 'bold')
        plt.show(block = False)
        self.fig.canvas.start_event_loop(timeout = 0)
        
        # Upon return of control, update last_shot and return
        self.last_shot = self.current_shot
        return self.last_shot
        
    def shot_in(self, opp_shot):
        """
        Evaluate an opponent shot as a hit or miss, and whether a ship was 
        sunk by the shot; updates the internal player board property with the
        new shot as well as any ship status if a ship is hit. 
        Returns a boolean tuple (shot_hit, ship_sunk). Does not modify the 
        input Shot object. 
        """
        
        # Returns a Ship if the shot is a hit; False otherwise.
        shot_check = self.player_board.shot_query(opp_shot)
        if shot_check:  # If a ship was returned this evalutes to True
            shot_hit = True
            shot_check.set_shot(opp_shot)
            ship_sunk = shot_check.is_sunk()
        else:
            shot_hit = False
            ship_sunk = False
            
            
        # Before plotting, update the "shot_hit" property. This violates the 
        # intent of the method by updating shot info early, but not a big 
        # deal for the human interface.
        opp_shot.hit = shot_hit
        
        # Print a status update on the opponent result
        print('\tOPPONENT: %s' % opp_shot)
        
        # Plot
        self.ax[1].clear()
        self.plot(ax = self.ax[1])
        plt.pause(0.01)
            
        return shot_hit, ship_sunk
        
    def shot_info(self, shot_hit, ship_sunk):
        """
        Supplies info to the agent on whether the last shot hit and sunk an 
        opponent ship. This will set the search or engage state of the 
        agent.
        """
        
        self.last_shot.set_hit(shot_hit)
        self.last_shot.set_sunk(ship_sunk)
        
        # Print result to command window
        print('\tRESULT: %s' % str(self.last_shot))
        
        # Plot the new shot on the opponent board
        self.ax[0].clear()
        self.plot_opp(self.ax[0])
        plt.pause(0.01)
        
            
    @classmethod
    def store_game(cls, game_result):
        """
        Stores a game in the internal "game_results" list.
        
        :param game_result: Single Game object added for storage.
        """
        
        cls.game_results.append(game_result)
            
    def _format_ship(self, raw_input):
        """
        Formats ship input from text. Input text should be
        [row label, col label, orientation].
        """
        
        # ValueError raised if this fails
        row_label, col_label, orientation = raw_input.strip().replace(' ', '').split(',')
        row_ind, col_ind = self._format_coords(row_label, col_label)
        orientation = orientation.upper()
        if orientation not in ['N', 'S', 'E', 'W']:
            raise ValueError
 
        return row_ind, col_ind, orientation
    
    def _format_shot(self, raw_input):
        """
        Formats ship input from text. Input text should be
        [row label, col label].
        """
        
        # ValueError raised if this fails
        row_label, col_label = raw_input.strip().replace(' ', '').split(',')
        return self._format_coords(row_label, col_label)
    
    def _format_coords(self, row_label, col_label):
        """
        Formats input row and column labels into indices.
        """
        row_label = row_label.upper()
        col_label = int(col_label)
        return display_coord(row_label, col_label)
    
if __name__ == '__main__':
    
    agent1 = Human_Agent()
    agent2 = AI_Agent()
    game_lengths = list()
    
    g = Game(agent1, agent2)
    g.setup()
    g.play()
    g.plot()
    plt.show()
