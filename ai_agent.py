# -*- coding: utf-8 -*-
"""

Baseline AI Agent and demonstation of a competition framework that can be used
to run games between two agents. The baseline AI agent makes decisions 
randomly; the only "intelligence" it has is switching to a separate engage
mode to try and sink ships once a hit occurs.

"""

# TODO: Potentially make competition framework class-based

import numpy as np
from random import randint, seed
from battleship import Agent, Ship, Shot, Game

class AI_Agent(Agent):
    """
    Concrete reference implementation of a very simple AI agent which simply
    takes actions at random.
    """
    
    # Class property for Game result storage
    game_results = list()
    
    def __init__(self, *args, **kwargs):
        
        # Superclass init
        super().__init__(*args, **kwargs)
        
        # Internal search/engage state
        self.state = 'search'  # Other valid choice is 'engage'
        self.engage_directions = ['N', 'S', 'E', 'W']
        self.engage_direction = 0
        self.engage_history = list()  # List of all shots taken during this 
                                      # engagement
    
    def place_ships(self, debug = False):
        """
        Places ships at random.
        """
        
        orientations = ['N', 'S', 'E', 'W']
        
        for ind, ship in enumerate(self.ships):
            
            if debug:  # Place ship in known locations
                row_ind = 2*ind
                col_ind = 0
                orientation = 'E'
                self.player_board.add_ship(ship, (row_ind, col_ind), 
                                           orientation)
                continue
            
            while True:
                
                row_ind = randint(0, self.player_board.board_rows - 1)
                col_ind = randint(0, self.player_board.board_cols - 1)
                orientation = orientations[randint(0, 3)]
                
                try:
                    self.player_board.add_ship(ship, (row_ind, 
                                                      col_ind), 
                                                      orientation)
                    break
                except ValueError:
                    continue
        
    def shot_out(self, current_turn, debug_loc = False):
        """
        Takes a shot at random. Returns a new Shot object. The new Shot object
        will be assigned to the "lastShot" property, and addd to the internal
        opponent board property. For debugging, assign a tuple of shot 
        coordinates "debug_loc" to set where the shot should be placed.
        """
        
        if debug_loc:
            new_shot = Shot(debug_loc, current_turn)
        elif self.state == 'search':
            new_shot = self._shot_search(current_turn)
        elif self.state == 'engage':
            new_shot = self._shot_engage(current_turn)
            self.engage_history.append(new_shot)
            
        self.opp_board.shot_set(new_shot)
        self.last_shot = new_shot
        return new_shot
        
    def shot_in(self, opp_shot):
        """
        Evaluate an opponent shot as a hit or miss, and whether a ship was 
        sunk by the shot; updates the internal player board property with the
        new shot as well as any ship status if a ship is hit. 
        Returns a boolean tuple (shot_hit, ship_sunk). Does not modify the 
        input Shot object. 
        """
        
        # Returns a Ship if the shot is a hit; False otherwise.
        shot_check = self.player_board.shot_query(opp_shot)
        if shot_check:  # If a ship was returned this evalutes to True
            shot_hit = True
            shot_check.set_shot(opp_shot)
            ship_sunk = shot_check.is_sunk()
        else:
            shot_hit = False
            ship_sunk = False
            
        return shot_hit, ship_sunk
        
    def shot_info(self, shot_hit, ship_sunk):
        """
        Supplies info to the agent on whether the last shot hit and sunk an 
        opponent ship. This will set the search or engage state of the 
        agent.
        """
        
        self.last_shot.set_hit(shot_hit)
        self.last_shot.set_sunk(ship_sunk)
        
        # Logic to change internal state from search to engage, and to set
        # the engagement direction.
        # If we are in search mode and have a hit, switch to engage and reset
        # the engage direction
        if shot_hit and self.state == 'search':
            self.state = 'engage'
            self.engage_direction = 0
            self.engage_history = [self.last_shot]
        # If we are in engage mode and last shot hit, go to the next possible
        # search direction
        elif (not shot_hit) and self.state == 'engage':
            self.engage_direction += 1
            if self.engage_direction > 3:  # Sanity check; this shouldn't occur
                self.state = 'search'
        # If the last shot sunk a ship, go back to search mode
        elif ship_sunk:
            self.state = 'search'
            
    @classmethod
    def store_game(cls, game_result):
        """
        Stores a game in the internal "game_results" list.
        
        :param game_result: Single Game object added for storage.
        """
        
        cls.game_results.append(game_result)
            
    def _shot_search(self, current_turn):
        """
        Fire a shot in "search" mode. Selected at random, so long as the 
        position has not been selected already.
        """
        
        while True:
            coords = (randint(0, self.player_board.board_rows - 1),
                      randint(0, self.player_board.board_cols - 1))
            new_shot = Shot(coords, current_turn)
            
            # If the shot on this location hasn't alrady been taken, return it
            if not self.opp_board.check_shot_exists(new_shot):
                return new_shot
            
    def _shot_engage(self, current_turn):
        """
        Fire a shot in "engage" mode. Sends a new shot relative to the 
        current direction as directed by the internal ship state. 
        """
        
        hits = [s for s in self.engage_history if s.hit]
        
        # Try and take a shot using current engage direction. If it's not a 
        # valid shot, increment and try again
        last_coords = hits[-1].get_coordinates()
        for i in range(self.engage_direction, 4):
            if i == 0:
                new_coords = (last_coords[0] + 1, last_coords[1])
            if i == 1:
                new_coords = (last_coords[0],     last_coords[1] + 1)
            if i == 2:
                new_coords = (last_coords[0] - 1, last_coords[1])
            if i == 3:
                new_coords = (last_coords[0],     last_coords[1] - 1)
            try:
                new_shot = Shot(new_coords, current_turn)
                if not self.opp_board.check_shot_exists(new_shot):
                    return new_shot
            except ValueError:
                continue
            
        # Repeat using the first shot taken during this engagement. If it's not
        # a valid shot, increment and try again
        last_coords = hits[0].get_coordinates()
        for i in range(self.engage_direction, 4):
            if i == 0:
                new_coords = (last_coords[0] + 1, last_coords[1])
            if i == 1:
                new_coords = (last_coords[0],     last_coords[1] + 1)
            if i == 2:
                new_coords = (last_coords[0] - 1, last_coords[1])
            if i == 3:
                new_coords = (last_coords[0],     last_coords[1] - 1)
            try:
                new_shot = Shot(new_coords, current_turn)
                if not self.opp_board.check_shot_exists(new_shot):
                    return new_shot
            except ValueError:
                continue
            
        # As a last resort, take a random shot if we get down here
        return self._shot_search(current_turn)
    
class AI_Agent2(AI_Agent):
    """
    Re-definition of AI Agent for the compete framework below, so that 
    separate class properties are available for storing games.
    """
    
    game_results = list()
    
    
def test_turns(count = 6):
    """
    Simulate a sequence of turns between two AI agents; this is similar to the code
    that is called in the Game class to run through turns. 
    """
    
    # Create agents and place ships
    p1 = AI_Agent()
    p2 = AI_Agent()
    p1.place_ships(debug = True)
    p2.place_ships(debug = True)
    
    # Simulate turns between player 1 and player 2
    for turn_number in range(count):
        
        # Player 1 takes shot
        if turn_number == 0:
            shot1 = p1.shot_out(turn_number, debug_loc = (0, 1))
        else:
            shot1 = p1.shot_out(turn_number)
        shot_hit, ship_sunk = p2.shot_in(shot1)
        p1.shot_info(shot_hit, ship_sunk)
        
        # Player 2 takes shot
        if turn_number == 0:
            shot2 = p2.shot_out(turn_number, debug_loc = (1, 0))
        else:
            shot2 = p2.shot_out(turn_number)
        shot_hit, ship_sunk = p1.shot_in(shot2)
        p2.shot_info(shot_hit, ship_sunk)
        
    return p1, p2

    
def test_game(plot = True, savestr = None, rng_seed = False):
    """
    Runs through a game between two AI Agents, optionally plotting the 
    game in progress, saving the images in progress, and seeding the RNG.
    """
    
    if rng_seed:
        seed(rng_seed)
        
    p1 = AI_Agent()
    p2 = AI_Agent()
    
    g = Game(p1, p2)
    g.setup()
    g.play(plot = plot, savestr = savestr)
    
if __name__ == '__main__':
    
    # test_game()
    
    # Run competition between two AI Agent classes. A new object is generated
    # at each turn. After each game, the Game result is appended to a class
    # property of each agent. 
    n_games = 1000
    agent1_wins = 0
    Agent1 = AI_Agent
    Agent2 = AI_Agent2
    game_lengths = list()
    
    for i in range(n_games):
        
        g = Game(Agent1(), Agent2())
        g.setup()
        g.play()
        
        if type(g.winner) == Agent1:
            agent1_wins += 1
            
        Agent1.store_game(g)
        Agent2.store_game(g)
        game_lengths.append(g.turn_number)
        
    print('Agent 1 wins %i of %i games' % (agent1_wins, n_games))
    print('Average Game Length: %i turns' % 
          (sum(game_lengths)/len(game_lengths)))