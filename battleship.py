# -*- coding: utf-8 -*-
"""

Supporting classes for Battleship simulation. 

These need to support:
    
    - Simulation between 2 AIs, with game state simulated
    - Play interactively against a player, with game state simulated
    - Play interactively against a player, with a physical game
    
Supporting classes are:
    
    - Shot: Info for each shot taken by the player
    - Ship: Info and state for each ship in the game
    - Board: Tracks the state of the game board for each agent.
    - Agent: Decision-making abstract base class. Can be AI or human driven.
    - Game: Drives the turn-taking game logic. Does not contain game state
    information; it is up to agents to properly report hits/misses, ship
    sinking, and loss of the game.


"""

# TODO: Write human-controlled agent
# TODO: Figure out how to make work with a physical game

import abc
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

def coord_display(coordinates):
    """
    Given integer board coordinates, return the letter/number
    designation of the location.
    
    :param coordinates: 2-tuple of integer board coordinates.
    """
    
    # TODO: Extend for boards bigger than 26
    # 65 is decimal index of unicode 'A'
    return '%s%i' % (chr(65 + coordinates[0]), coordinates[1] + 1)

def display_coord(row_label, col_label):
    """
    Convert from letter/number to coordinates.
    """
    # 65 is decimal index of unicode 'A'
    return (ord(row_label) - 65, col_label - 1)
    

class Agent(abc.ABC):
    """
    Abstract base class for decision-making agents. Can be inherited to 
    generate either human or AI-controlled agents.
    """

    def __init__(self, *args, board_rows = 10, board_cols = 10, **kwargs):
        """
        Initialize object; generate empty starting boards for player and 
        opponent.
        
        :param board_rows: Number of rows (letter indexed) on the board
        :param board_cold: Number of columns (numbe indexed) on the board
        """
        
        self.player_board = Board(board_rows, board_cols)
        self.opp_board = Board(board_rows, board_cols)
        self.ships = [Ship.make_battleship(),
                      Ship.make_carrier(),
                      Ship.make_cruiser(),
                      Ship.make_destroyer(),
                      Ship.make_submarine()]
        self.last_shot = Shot((0, 0), 0)  # Dummy shot object
        
    @abc.abstractmethod
    def place_ships(self):
        """
        Call this method to place ships before the game starts. The ships 
        must be added to the self.player_board property.
        """
        
    @abc.abstractmethod
    def shot_out(self, current_turn):
        """
        Request a shot call for this agent's turn. Returns a Shot object
        which will be evaluated by the opponent agent as a hit or a miss.
        
        :param current_turn: Turn number of the shot.
        :return: A Shot object that the Agent is taking this turn.
        """
        
    @abc.abstractmethod
    def shot_in(self, opp_shot):
        """
        Evaluate an opponent shot as a hit or miss, and whether a ship was 
        sunk by the shot. Returns a boolean tuple (shot_hit, ship_sunk). Does
        not modify the input Shot object.
        
        :param opp_shot: Shot object representing the opponent shot 
        :return: (shot_hit, ship_sunk) -- Boolean values describing whether a 
        ship was hit and whether the hit sunk the ship.
        """
        
    @abc.abstractmethod
    def shot_info(self, shot_hit, ship_sunk):
        """
        Supplies info to the agent on whether the last shot hit and sunk an 
        opponent ship.
        
        :param shot_hit: Boolean variable describing whether a ship was hit
        :param shot_sunk: Boolean variable describing whether a ship was sunk
        """
        
    def check_loss(self):
        """
        Checks if the agent has lost the game.
        """
        
        return all([s.is_sunk() for s in self.ships])
    
    def plot(self, ax = None):
        """
        Plots current state using player board.
        """
        
        self.player_board.plot(ax)
        
    def plot_opp(self, ax = None):
        """
        Plots current opponent board state.
        """
        
        self.opp_board.plot(ax)
        
        
class Board:
    """
    Tracks state of the game board. Board is indexed with (0, 0) at upper left.
    Row indices are displayed as letters; column indices displayed as numbers.
    """
    
    def __init__(self, board_rows = 10, board_cols = 10):
        """
        Initialize board; set dimensions and create empty arrays for Ship
        storage.
        """
        
        # Board settings
        self.board_rows = board_rows
        self.board_cols = board_cols
        
        # Create arrays for tracking shots and ship locations. Both are object
        # arrays; ship_array contains mixed boolean and Ship values.
        self.shot_array, self.ship_array = self._generate_board(self.board_rows,
                                                                self.board_cols)  
        
        # Ship settings
        self.nship = 0
        self.ships = list()
        
    def add_ship(self, new_ship, location, orientation):
        """
        Adds a Ship object to the board at the specified location (2-tuple of 
        integer coordinates) and orientation (string; 'N', 'S', 'E', 'W'). This
        method will alter the ship's location and orientation properties and
        obtain the masked coordinates from the ship. If the ship would go off
        the edge of the board, raises ValueError.
        """
        
        self.nship += 1
        self.ships.append(new_ship)
        
        new_ship.set_location(location)
        new_ship.set_orientation(orientation)
        
        ship_coords = new_ship.get_coordinates()
        if any([not self.check_coords(s) for s in ship_coords]):
            raise ValueError('Ship %s out of bounds' % str(new_ship))
        ship_inds = self.coords2ind(ship_coords)
            
        # Check if a ship already exists 
        if any(self.ship_array[ship_inds]):
            raise ValueError('Ship %s intersects with existing ship' % str(new_ship))
        
        # Otherwise assign to ship array
        self.ship_array[ship_inds] = new_ship
        
    def shot_query(self, new_shot):
        """
        Passes a shot object and requests the status of the shot. If a ship was
        hit, returns the cooresponding Ship object without updating the 
        internal status of the ship. Returns False otherwise. If the shot
        is off the board, raise ValueError.
        """
        
        shot_coord = new_shot.get_coordinates()
        shot_ind = self.coord2ind(shot_coord)
        if self.check_shot_exists(new_shot):  # Also checks index
            raise ValueError('%s taken on existing %s' % 
                             (str(new_shot), str(self.shot_array[shot_ind])))
            
        self.shot_array[shot_ind] = new_shot
        return self.ship_array[shot_ind]
    
    def shot_set(self, new_shot):
        """
        Sets shot object location without requesting info on whether a ship
        was hit or not (used to set status on opponent board where ship
        locations are not known). If the shot is off the board, raise
        ValueError.
        """
        
        shot_coord = new_shot.get_coordinates()
        shot_ind = self.coord2ind(shot_coord)
        if self.check_shot_exists(new_shot):  # Also checks index
            raise ValueError('%s taken on existing %s' % 
                             (str(new_shot), str(self.shot_array[shot_ind])))
            
        self.shot_array[shot_ind] = new_shot
        
    def check_coords(self, coord):
        """
        Given a tuple of coordinates, ensure this is a valid location on the
        board.
        """
        
        if ((coord[0] < 0) or (coord[0] >= self.board_rows) or
            (coord[1] < 0) or (coord[1] >= self.board_cols)): 
            return False
        else:
            return True
         
    def check_shot_exists(self, new_shot):
        """
        Checks if a shot location has already been taken. Returns True if so.
        Returns False otherwise.
        """
        
        shot_coord = new_shot.get_coordinates()
        if not self.check_coords(shot_coord):
            raise ValueError('%s out of bounds' % str(new_shot))
            
        shot_ind = self.coord2ind(shot_coord)
        if self.shot_array[shot_ind]:
            return True
        else:
            return False
        
    def plot(self, ax = None):
        """
        Plots the board, ships, and shots at the current state on either an 
        optionally supplied axis or a new figure (if no axis is provided).
        """
        
        # Handle the axis
        if not ax: 
            fig, ax = plt.subplots(1, 1)
            fig.canvas.set_window_title('BATTLESHIP')
        
        # Plot the board squares
        vlines = range(self.board_cols + 1)
        hlines = range(self.board_rows + 1)
        ax.vlines(vlines, hlines[0], hlines[-1])
        ax.hlines(hlines, vlines[0], vlines[-1])
        ax.set_xticks(0.5 + np.array(vlines)[:-1])
        ax.set_xticklabels(np.array(vlines) + 1)
        ax.set_yticks(0.5 + np.array(hlines)[:-1])
        ax.set_yticklabels([chr(65 + s) for s in np.array(hlines)[:-1]])
        
        # Plot the ships
        for ship in self.ships:
            ship.plot(ax)
        
        
        # Plot the shots
        shots = [s for s in self.shot_array if isinstance(s, Shot)]
        for shot in shots:
            shot.plot(ax)
        
        ax.set_xlim(vlines[0] - 0.25, vlines[-1] + 0.25)
        ax.set_ylim(hlines[0] - 0.25, hlines[-1] + 0.25)
        ax.invert_yaxis()
        
    def _generate_board(self, board_rows, board_cols):
        """
        Generates board arays. Returns object arrays which are initially 
        populated to None (shot_array) and False (ship_array).
        """
        
        shot_array = np.zeros(board_rows*board_cols, dtype = object)
        ship_array = np.zeros(board_rows*board_cols, dtype = object)
        
        shot_array[:] = None
        ship_array[:] = False
        
        return shot_array, ship_array
    
    def coord2ind(self, coord):
        """
        Given a 2-tuple, return single 1-D index into shot/ship array.
        """
        
        return coord[0]*self.board_rows + coord[1]
    
    def coords2ind(self, coords):
        """
        Converts a list of 2-tuples (board coordinates) to the 1-D indices
        used to access items in the shot array and ship array. 
        """
        
        ninds = len(coords)
        indices = np.zeros(ninds, dtype = int)
        for ind, coord in enumerate(coords):
            indices[ind] = self.coord2ind(coord)
        return indices    
            
    
class Ship:
    """
    Class for all ship objects.
    """
    
    def __init__(self, ship_name, ship_size):
        
        assert isinstance(ship_name, str)
        assert isinstance(ship_size, int)
        assert ship_size > 0
        
        self.name = ship_name.upper()  # ALL CAPITALS
        self.size = ship_size
        
        self.location = None
        self.orientation = None
        self.coordinates = [tuple()]*self.size
        
        self.nhits = 0
        self.hits = [False]*self.size
        
        # Plotting
        self.shape = None
        
    def compute_coordinates(self):
        """
        Uses stored location and orientation to get ship coordinates.
        """
        
        if self.orientation in ['N', 'n']:
            next_index = lambda coords: (coords[0] - 1, coords[1])
        if self.orientation in ['S', 's']:
            next_index = lambda coords: (coords[0] + 1, coords[1])
        if self.orientation in ['E', 'e']:
            next_index = lambda coords: (coords[0], coords[1] + 1)
        if self.orientation in ['W', 'w']:
            next_index = lambda coords: (coords[0], coords[1] - 1)
            
        self.coordinates[0] = self.location
        for ind in range(1, self.size):
            self.coordinates[ind] = next_index(self.coordinates[ind - 1])
            
    def get_coordinates(self):
        """
        Returns coordinates of the ship which can be used to index into the
        board arrays.
        """
        return self.coordinates
        
    def is_sunk(self):
        """
        Returns True if ship is sunk.
        """
        
        return all(self.hits)
        
    def set_location(self, location):
        """
        Sets ship location using 2-tuple of board coordinates. The location
        is the spot where the "front" of the ship is placed. If both location
        and orientation have been set, builds the internal coordiante array.
        """
        
        assert len(location) == 2
        assert isinstance(location[0], int) and isinstance(location[1], int)
        assert location[0] >= 0 and location[1] >= 0
        self.location = location
        
        if self.orientation is not None:
            self.compute_coordinates()
            
    def set_orientation(self, orientation):
        """
        Sets ship orientation as 'N' 'S' 'E' or 'W', which indicates the 
        direction that the rest of the ship is If both location
        and orientation have been set, builds the internal coordiante array.
        """
        
        assert orientation in ['N', 'S', 'E', 'W', 'n', 's', 'e', 'w']
        self.orientation = orientation
        
        if self.location is not None:
            self.compute_coordinates()
            
    def set_shot(self, shot_in):
        """
        Updates the ship status using the supplied shot_in. If the shot is 
        located on a ship coordinate, that spot is set to "hit" within the
        ship internal status.
        """
        
        shot_coords = shot_in.get_coordinates()
        if shot_coords in self.coordinates:
            shot_index = self.coordinates.index(shot_coords)
            self.hits[shot_index] = True
            self.nhits += 1
                        
    def plot(self, ax = None):
        """
        Plots ship on the supplied axis.
        """
        
        # Plotting properties
        offset = 0.1
        angles = {'N': 0, 'S': 0, 'E': 90, 'W': 90}
        
        # Handle the axis
        if not ax: 
            fig, ax = plt.subplots(1, 1)
            
        # Clear self if needed
        if self.shape is not None:
            self.shape.remove()
            
        # Make a rectangle. 
        corner_inds = self.get_coordinates()
        x_inds = np.array([s[1] for s in corner_inds])
        y_inds = np.array([s[0] for s in corner_inds])
        length = self.size - 2*offset
        width = 1. - 2*offset
        x_center = np.mean(x_inds) + 0.5
        y_center = np.mean(y_inds) + 0.5
        self.shape = Ellipse((x_center, y_center), 
                                  width, length, color = 'k',
                                  alpha = 0.6, 
                                  angle = angles[self.orientation])
        ax.add_patch(self.shape)
        
    def __str__(self):
        
        outstr = '%s (size %i)' % (self.name, self.size)
        if (self.location is not None) and (self.orientation is not None):
            outstr += ' at square %s; orientation %s' % (coord_display(self.location),
                                                         self.orientation)
        outstr += '; %i/%i hits' % (self.nhits, self.size)
        
        return outstr
    
    __repr__ = __str__
            
    # Construction methods
    @staticmethod
    def make_battleship():
        """
        Constructs and returns a BATTLESHIP.
        """
        
        return Ship('BATTLESHIP', 4)
    
    @staticmethod
    def make_carrier():
        """
        Constructs and returns a CARRIER.
        """
        
        return Ship('CARRIER', 5)
    
    @staticmethod
    def make_cruiser():
        """
        Constructs and returns a CRUISER.
        """
        
        return Ship('CRUISER', 3)

    @staticmethod
    def make_destroyer():
        """
        Constructs and returns a DESTROYER.
        """
        
        return Ship('DESTROYER', 2)
    
    @staticmethod
    def make_submarine():
        """
        Constructs and returns a SUBMARINE.
        """
        
        return Ship('SUBMARINE', 3)


class Shot:
    """
    Tracks info for each shot taken during the game.
    """
    
    def __init__(self, target_coord, turn_number):
        """
        Create shot using 2-tuple target coordinate and the current 
        turn number.
        """
        
        self.target_display = coord_display(target_coord)
        self.target_coord = target_coord
        self.turn_number = turn_number
        
        # Assigned later
        self.hit = None
        self.sunk = None
        
        # Plotting
        self.shape = None
        self.text = None
        
    def get_coordinates(self):
        """
        Return 2-tuple of shot coordinates.
        """
        
        return self.target_coord
    
    def set_coordinates(self, target_coord):
        """
        Sets the shot coordinates to the new target coordinates.
        """
        
        self.target_coord = target_coord
        
    def set_hit(self, status):
        """
        Sets hit status to True or False
        """
        
        assert isinstance(status, bool)
        self.hit = status
        
    def set_sunk(self, status):
        """
        Sets sunk status to True or False.
        """
        
        assert isinstance(status, bool)
        self.sunk = status
        
    def plot(self, ax = None):
        """
        Plots shot on the supplied axis.
        """
        
        offset = 0.2
        # Handle the axis
        if not ax: 
            fig, ax = plt.subplots(1, 1)
            
        # Clear self if needed
        if self.shape is not None:
            self.shape.remove()
            self.text.remove()
            
        # Make a circle. 
        center_inds = self.get_coordinates()
        diameter = 1. - 2*offset
        x_center = center_inds[1] + 0.5
        y_center = center_inds[0] + 0.5
        if self.hit:
            color = 'r'
        else:
            color = 'w'
        if self.sunk:
            fontweight = 'bold'
        else:
            fontweight = 'normal'
            
        self.shape = Ellipse((x_center, y_center), 
                                  diameter, diameter, facecolor = color,
                                  edgecolor = 'k')
        ax.add_patch(self.shape)
        self.text = ax.text(x_center, y_center, '%s' % (self.turn_number + 1),
                verticalalignment = 'center',
                horizontalalignment = 'center',
                fontweight = fontweight)
        
    def __str__(self):
        
        outstr = 'Shot; Turn %i; Target: %s' % (self.turn_number,
                                                self.target_display)
        if self.hit is None:
            return outstr
        elif self.hit:
            outstr += ' HIT'
            if self.sunk:
                outstr += ' AND SUNK'
        else:
            outstr += ' MISS'
        
        return outstr
    
    __repr__ = __str__
        
    
class Game:
    """
    Top-level driver class for a game between two agents.
    """
    
    def __init__(self, agent1, agent2):
        """
        Initialize the class with 2 agents. These should be un-initialized; 
        no ships placed yet. Further initialization for the agents takes place
        in the "setup" method.
        """
        
        self.agent1 = agent1
        self.agent2 = agent2
        self.turn_number = 0
        
        # Store lists of the shots each agent takes, for convenience
        self.agent1_shots = list()
        self.agent2_shots = list()
        
        # Store winner and loser
        self.winner = None
        self.loser = None
        
    def advance_turn(self, turn_number):
        """
        Completes a single turn.
        """
        
        self.turn_number += 1
        
        # Agent 1 takes shot
        shot1 = self.agent1.shot_out(turn_number)
        shot_hit, ship_sunk = self.agent2.shot_in(shot1)
        self.agent1.shot_info(shot_hit, ship_sunk)
        self.agent1_shots.append(shot1)
        
        # If Agent 2 has lost after this shot, return early. Duplicates a 
        # later check but who cares.
        if self.agent2.check_loss():
            return
        
        # Agent 2 takes shot
        shot2 = self.agent2.shot_out(turn_number)
        shot_hit, ship_sunk = self.agent1.shot_in(shot2)
        self.agent2.shot_info(shot_hit, ship_sunk)
        self.agent2_shots.append(shot2)
        
    def play(self, plot = False, plot_opp = False, savestr = False):
        """
        Plays a game. Game proceeds until a winner is declared, at which point
        the "winner" and "loser" properties are assigned as the corresponding
        agents. 
        """
        
        if plot:
            fig, ax = plt.subplots(1, 2, figsize = (12, 6))
            fig.canvas.set_window_title('BATTLESHIP')
            
        if plot_opp:  # Plots Player 1's player and opponent boards
            fig, ax = plt.subplots(2, 1, figsize = (5, 10))
            fig.canvas.set_window_title('BATTLESHIP')
        
        turn_number = 0
        while True:
            self.advance_turn(turn_number)
            turn_number += 1
            
            if plot:
                ax[0].clear()
                ax[1].clear()
                self.agent1.plot(ax[0])
                self.agent2.plot(ax[1])
                plt.pause(0.05)
                
                if savestr:
                    plt.savefig(savestr % turn_number, bbox_inches = 'tight')
                    
            if plot_opp:
                ax[0].clear()
                ax[1].clear()
                self.agent1.plot_opp(ax[0])
                self.agent1.plot(ax[1])
                plt.pause(0.05)
                
                if savestr:
                    plt.savefig(savestr % turn_number, bbox_inches = 'tight')
                
            
            # Check if agent 2 has lost the game (agent 1 goes first and gets
            # priority)
            if self.agent2.check_loss():
                self.winner = self.agent1
                self.loser = self.agent2
                return
            
            if self.agent1.check_loss():
                self.winner = self.agent2
                self.loser = self.agent1
                return
            
    def setup(self):
        """
        Sets up the game by having both agents place their ships.
        """
        
        self.agent1.place_ships()
        self.agent2.place_ships()
        
    def plot(self):
        """
        Plot the current stored state of the game
        """
        
        fig, ax = plt.subplots(1, 2, figsize = (12, 6))
        fig.canvas.set_window_title('BATTLESHIP')
        self.agent1.plot(ax[0])
        self.agent2.plot(ax[1])
        
        
   